package main

import (
	"fmt"
	"math/rand"
	"strings"
	"time"
	"unicode"
)

var (
	statsPing, statsPoll, statsRemindme, statsCoinflip, statsDice int
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func ez27EmojiDigit(digit int) string {

	emojis := []string{"0️⃣", "1️⃣", "2️⃣", "3️⃣", "4️⃣", "5️⃣", "6️⃣", "7️⃣", "8️⃣", "9️⃣"}

	if digit >= 0 && digit <= 9 {
		return emojis[digit]
	}

	return ""
}

func ez27CommandsHelp() string {

	response := "```\n"

	response += "ez!ping\n"
	response += "ez!poll\n"
	response += "ez!poll2\n"
	response += "ez!remindme\n"
	response += "ez!coin\n"
	response += "ez!dice\n"
	response += "\n"
	response += "ez!amongus\n"

	return response + "\n```"
}

func ez27FormatUsage(command, usage string) string {

	return "`" + command + " " + usage + "`"
}

func ez27RunCommand(command string) (string, []string) {

	quoteOpened := false
	fields := strings.FieldsFunc(command, func(c rune) bool {
		if c == '"' {
			quoteOpened = !quoteOpened
			return true
		}
		return unicode.IsSpace(c) && !quoteOpened
	})

	// Base functions
	switch fields[0] {
	case "ez!stats":
		return ez27RunStats(fields)
	case "ez!ping":
		return ez27RunPing(fields)
	case "ez!poll":
		return ez27RunPoll(fields)
	case "ez!poll2":
		return ez27RunPollLong(fields)
	case "ez!remindme":
		return ez27RunRemindMe(fields)
	case "ez!coin":
		return ez27RunCoinFlip(fields)
	case "ez!dice":
		return ez27RunDice(fields)
	}

	// Aliases
	switch fields[0] {
	case "ez!amongus":
		return ez27RunPoll([]string{fields[0], "Qui est chaud pour un Among Us ?", "Vendredi", "Samedi", "Dimanche"})
	}

	return ez27CommandsHelp(), nil
}

func ez27RunStats(argv []string) (string, []string) {

	response := "```"
	response += "Alive for " + time.Since(StartTime).Truncate(time.Second).String() + "\n"
	response += fmt.Sprintf("%d pings pong'd\n", statsPing)
	response += fmt.Sprintf("%d polls conducted\n", statsPoll)
	response += fmt.Sprintf("%d things reminded\n", statsRemindme)
	response += fmt.Sprintf("%d coins flipped\n", statsCoinflip)
	response += fmt.Sprintf("%d dices rolled\n", statsDice)
	response += "```"

	return response, nil
}

func ez27RunPing(argv []string) (string, []string) {

	statsPing++
	return "Pong!", nil
}

func ez27RunPoll(argv []string) (string, []string) {

	if len(argv) < 4 || len(argv) > 10 {
		return ez27FormatUsage(argv[0], "<title> <answers> (min 2 answers, up to 8)"), nil
	}

	response := argv[1] + "\n"
	reaction := make([]string, len(argv[2:]))

	for i, answer := range argv[2:] {
		response += "`" + ez27EmojiDigit(i+1) + " " + answer + "` "
		reaction = append(reaction, ez27EmojiDigit(i+1))
	}

	statsPoll++
	return response, reaction
}

func ez27RunPollLong(argv []string) (string, []string) {

	if len(argv) < 4 || len(argv) > 10 {
		return ez27FormatUsage(argv[0], "<title> <answers> (min 2 answers, up to 8)"), nil
	}

	response := argv[1] + "\n"
	reaction := make([]string, len(argv[2:]))

	response += "```"
	for i, answer := range argv[2:] {
		response += "\n" + ez27EmojiDigit(i+1) + " " + answer
		reaction = append(reaction, ez27EmojiDigit(i+1))
	}
	response += "\n```"

	statsPoll++
	return response, reaction
}

func ez27RunRemindMe(argv []string) (string, []string) {

	if len(argv) > 2 {
		duration, err := time.ParseDuration(argv[1])
		if err == nil {
			time.Sleep(duration)
			statsRemindme++
			return "Let me remind you this\n> " + argv[2], nil
		}
	}

	return ez27FormatUsage(argv[0], "<duration> <something> (specified with h, m, s, eg 1h33m47s)"), nil
}

func ez27RunCoinFlip(argv []string) (string, []string) {

	statsCoinflip++
	random := rand.Intn(2)
	if random == 0 {
		return "Heads!", nil
	}

	return "Tails!", nil
}

func ez27RunDice(argv []string) (string, []string) {

	statsDice++
	return ez27EmojiDigit(rand.Intn(6) + 1), nil
}
